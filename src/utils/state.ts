type Key = string | number;
type getStateFunc<T> = (state: object, keyPath: Key[] | Key, defaultValue?: T) => T;

export const getState: getStateFunc<any> = (state = {}, keyPath, defaultValue) => {
  if (typeof keyPath === 'string' || typeof keyPath === 'number') {
    keyPath = [keyPath];
  }
  const key = keyPath.shift() as Key;
  let subState = state[key] ?? {};
  if (keyPath.length === 0) {
    return state[key] ?? defaultValue;
  }
  if (typeof subState !== 'object') {
    subState = {};
  }
  return getState(subState as object, keyPath, defaultValue);
};

type getStateInFunc<T> = (
  state: object,
  keyPath: Key[] | Key,
  fn: (subState: any) => T,
  defaultValue?: T,
) => T;

export const getStateIn: getStateInFunc<any> = (
  state = {},
  keyPath,
  fn,
  defaultValue,
) => {
  if (typeof keyPath === 'string' || typeof keyPath === 'number') {
    keyPath = [keyPath];
  }
  const key = keyPath.shift() as Key;
  let subState = state[key] ?? {};
  if (keyPath.length === 0) {
    try {
      return fn(state[key]);
    } catch (e: unknown) {
      return defaultValue;
    }
  }
  if (typeof subState !== 'object') {
    subState = {};
  }
  return getStateIn(subState as object, keyPath, fn, defaultValue);
};

export const updateState = (
  state: object = {},
  keyPath: Key[] | Key,
  value: any,
): typeof state => {
  if (typeof keyPath === 'string' || typeof keyPath === 'number') {
    keyPath = [keyPath];
  }
  if (keyPath.length === 0) {
    return value;
  }
  const key = keyPath.shift() as Key;
  let subState = state[key] ?? {};
  if (typeof subState !== 'object') {
    subState = {};
  }
  return {
    ...state,
    [key]: updateState(subState as object, keyPath, value),
  };
};

export const updateStateIn = (
  state: object = {},
  keyPath: Key[] | Key,
  fn: Function,
): typeof state => {
  if (typeof keyPath === 'string' || typeof keyPath === 'number') {
    keyPath = [keyPath];
  }
  const key = keyPath.shift() as Key;
  if (keyPath.length === 0) {
    try {
      return {
        ...state,
        [key]: fn(state[key]),
      };
    } catch (e: unknown) {
      return state;
    }
  }
  let subState = state[key] ?? {};
  if (typeof subState !== 'object') {
    subState = {};
  }
  return {
    ...state,
    [key]: updateStateIn(subState as object, keyPath, fn),
  };
};

export const removeState = (
  state: object = {},
  keyPath: Key[] | Key,
): typeof state => {
  if (typeof keyPath === 'string' || typeof keyPath === 'number') {
    keyPath = [keyPath];
  }
  const key = keyPath.shift() as Key;
  if (keyPath.length === 0) {
    return Object.keys(state).reduce((m, n) => {
      if (n === key) return m;
      m[n] = state[n];
      return m;
    }, {});
  }
  const subState = removeState((state[key] ?? {}) as object, keyPath);
  // remove key if it doesnot has any child
  if (Object.keys(subState).length === 0) {
    return Object.keys(state).reduce((m, n) => {
      if (n === key) return m;
      m[n] = state[n];
      return m;
    }, {});
  }
  return {
    ...state,
    [key]: subState,
  };
};

