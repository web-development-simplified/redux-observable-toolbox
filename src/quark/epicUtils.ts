import { Epic, StateObservable } from 'redux-observable';
import { Observable } from 'rxjs';
import { ajax } from 'rxjs/ajax';

type Method = 'GET' | 'POST' | 'PUT' | 'DELETE';

export const createDependencies = ({
  dependencies = {},
  headers = (
    action$: Observable<any>,
    state$: StateObservable<any>,
    method: Method,
    url: string,
    data: object,
    options: object,
  ) => ({
    'Content-Type': 'application/json',
  }),
}) => ({
  dependencies: {
    get: (
      action$: Observable<any>,
      state$: StateObservable<any>,
      url: string,
      data: object,
      options: object,
    ) => {
      const pdata = Object.keys(data).reduce((meno, key) => {
        if (key && data[key] != null && data[key] != '') {
          return { ...meno, [key]: data[key] };
        }
        return meno;
      }, {});
      const params = new URLSearchParams(pdata);
      return ajax({
        url: `${url}${url.includes('?') ? '&' : '?'}${params.toString()}`,
        method: 'GET',
        headers: headers(action$, state$, 'GET', url, data, options),
      });
    },
    post: (
      action$: Observable<any>,
      state$: StateObservable<any>,
      url: string,
      data: object,
      options: object,
    ) => {
      return ajax({
        url,
        method: 'POST',
        headers: headers(action$, state$, 'POST', url, data, options),
        body: data,
      });
    },
    put: (
      action$: Observable<any>,
      state$: StateObservable<any>,
      url: string,
      data: object,
      options: object,
    ) => {
      return ajax({
        url,
        method: 'PUT',
        headers: headers(action$, state$, 'PUT', url, data, options),
        body: data,
      });
    },
    del: (
      action$: Observable<any>,
      state$: StateObservable<any>,
      url: string,
      data: object,
      options: object,
    ) => {
      return ajax({
        url,
        method: 'DELETE',
        headers: headers(action$, state$, 'DELETE', url, data, options),
        body: data,
      });
    },
    ...dependencies,
  },
});

export const injectToDependencies = (rootEpic: Epic): Epic => {
  return (action$, state$, dependencies: object = {}) => {
    for (const dependency in dependencies) {
      if (dependencies.hasOwnProperty(dependency)) {
        const dependencyValue = dependencies[dependency];

        if (typeof dependencyValue === 'function') {
          dependencies[dependency] = (...args) =>
            dependencyValue(action$, state$, ...args);
        }
      }
    }

    return rootEpic(action$, state$, dependencies);
  };
};
