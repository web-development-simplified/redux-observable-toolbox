import {
  ActionReducerMapBuilder,
  createSlice,
  PayloadAction,
  Slice,
  SliceCaseReducers,
} from '@reduxjs/toolkit';
import {
  mergeMap,
  map,
  takeUntil,
  filter,
  startWith,
  endWith,
  throttleTime,
} from 'rxjs/operators';
import { combineLatest, of, catchError, Observable } from 'rxjs';
import { combineEpics, Epic, type StateObservable } from 'redux-observable';
import {
  getState,
  updateState,
  updateStateIn,
  removeState,
  getStateIn,
} from '../utils/state';
import { AjaxResponse } from 'rxjs/internal/ajax/AjaxResponse';

type IdAttr = string | number;

interface CreateModuleArgs {
  name: string;
  resourceUrl: string;
  resourceType: string;
  initialState?: object | ((obj: object) => object);
  reducers?:
    | SliceCaseReducers<object>
    | ((obj: SliceCaseReducers<object>) => SliceCaseReducers<object>);
  epics?: EpicMap | ((epics: EpicMap) => EpicMap);
  ajaxThrottleTime?: number;
  selectors?: Selectors<object> | ((obj: Selectors<object>) => Selectors<object>);
  // A "builder callback" function used to add more reducers, or
  // an additional object of "case reducers", where the keys should be other
  // action types
  extraReducers?:
    | ((builder: ActionReducerMapBuilder<object>) => void);
}

interface ModuleOption {
  slice: Slice;
  epic: Epic;
  name: string;
  actions: Slice['actions'];
  reducer: Slice['reducer'];
  selectors: object;
}

interface Selectors<State extends object> {
  [key: string]: ((state: State) => object) | ((a: any) => (state: State) => object);
}

interface EpicMap {
  [key: string]: Epic;
}

export const createModule = ({
  name,
  resourceUrl,
  resourceType,
  initialState = {},
  reducers,
  selectors,
  epics,
  ajaxThrottleTime = 1000,
  extraReducers,
}: CreateModuleArgs): ModuleOption => {
  let _initialState: object = {
    listData: [],
    total: 0,
    current: 1,
    pageSize: 10,
    selectedData: [],
    searchData: {},
    orderby: '',
    syncing: false,
    syncingItems: {},
    creating: false,
    updatingItems: {},
    removingItems: [],
    syncingAll: false,
    dataMap: {},
    moduleParams: {},
  };

  if (initialState) {
    if (typeof initialState === 'function') {
      _initialState = initialState(_initialState);
    } else {
      _initialState = { ..._initialState, ...initialState };
    }
  }

  let _selectors: Selectors<object> = {
    selectModule: (state) => getState(state, [name], _initialState),
    selectModuleParams: (state) => getState(state, [name, 'moduleParams'], {}),
    selectPagination: (state) => ({
      current: getState(state, [name, 'current'], 1),
      pageSize: getState(state, [name, 'pageSize'], 10),
      total: getState(state, [name, 'total'], 0),
    }),
    selectPageData: (state) =>
      getStateIn(state, [name, 'listData'], (list: IdAttr[]) => {
        return list
          .map((id: IdAttr) => getState(state, [name, 'dataMap', id]))
          .filter(Boolean);
      }),
    selectSelected: (state) => getState(state, [name, 'selectedData'], []),
    selectSelectedData: (state) => {
      return getStateIn(
        state,
        [name, 'selectedData'],
        (ids: IdAttr[]) =>
          ids.map((id) => getState(state, [name, 'dataMap', id])).filter(Boolean),
        [],
      );
    },
    selectSearchData: (state) => getState(state, [name, 'searchData'], {}),
    selectOrderby: (state) => getState(state, [name, 'orderby'], ''),

    selectSyncing: (state) => getState(state, [name, 'syncing'], false),
    selectItemSyncing: (id: IdAttr) => (state) =>
      getState(state, [name, 'syncingItems', id], false),
    selectCreating: (state) => getState(state, [name, 'creating'], false),
    selectItemUpdating: (id: IdAttr) => (state) =>
      getState(state, [name, 'updatingItems', id], false),
    selectItemRemoving: (id: IdAttr) => (state) =>
      getStateIn(
        state,
        [name, 'removingItems'],
        (ids: IdAttr[]) => ids.includes(id),
        false,
      ),
    selectAllSyncing: (state) => getState(state, [name, 'syncingAll'], false),
    selectItemSyncStatus: (id: IdAttr) => {
      return (state) => {
        return {
          reading: getState(state, [name, 'syncingItems', id], false),
          updating: getState(state, [name, 'updatingItems', id], false),
          removing: getStateIn(
            state,
            [name, 'removingItems'],
            (ids: IdAttr[]) => ids.includes(id),
            false,
          ),
        };
      };
    },

    selectAllData: (state) => getState(state, [name, 'dataMap'], {}),
    selectAllDataList: (state) =>
      getStateIn(
        state,
        [name, 'dataMap'],
        (dm) => {
          return Object.keys(dm).map((id: IdAttr) => dm[id]);
        },
        [],
      ),
    selectAllDataOptions: (state) => {
      return getStateIn(state, [name, 'dataMap'], (dm) => {
        return Object.keys(dm).map((d) => ({
          name: getState(dm, [d, 'name']),
          value: d,
        }));
      });
    },
    selectOneItem: (id: IdAttr) => (state) => getState(state, [name, 'dataMap', id], {}),
    selectItemByIDList: (idList: IdAttr[]) => (state) =>
      getStateIn(
        state,
        [name, 'dataMap'],
        (dm) => {
          return idList.map((id) => getState(dm, [id], {}));
        },
        [],
      ),
  };

  if (selectors) {
    if (typeof selectors === 'function') {
      _selectors = selectors(_selectors);
    } else {
      _selectors = { ..._selectors, ...selectors };
    }
  }

  let _reducers: SliceCaseReducers<object> = {
    updateModuleParams: (state, action) =>
      updateState(state, ['moduleParams'], action.payload),

    updateCurrent: (state, action) => updateState(state, ['current'], action.payload),
    updatePageSize: (state, action) => updateState(state, ['pageSize'], action.payload),

    updateSelectedData: (state, action) =>
      updateState(state, ['selectedData'], action.payload),

    updateSearch: (state, action) => updateState(state, ['searchData'], action.payload),

    updateOrderBy: (state, action) => updateState(state, ['orderby'], action.payload),

    sync: (state, action) => state,
    syncStarted: (state, action) => updateState(state, ['syncing'], true),
    syncEnded: (state, action) => updateState(state, ['syncing'], false),
    syncFulfilled: (state, { payload: { data, count } }) => {
      state = updateState(
        state,
        ['listData'],
        data.map((d) => d.id),
      );
      state = updateStateIn(state, ['selectedData'], (idList) =>
        idList.filter((id) => getState(state, ['listData'], []).includes(id)),
      );
      state = updateState(state, ['total'], count);
      state = updateStateIn(state, ['dataMap'], (dm) =>
        data.reduce((suc, i) => ({ ...suc, [i.id]: i }), dm),
      );
      return state;
    },
    syncRejected: (state, action) => state,
    cancelSync: (state, action) => state,

    syncItem: (state, action) => state,
    syncItemStarted: (state, { payload: { id } }) =>
      updateState(state, ['syncingItems', id], true),
    syncItemEnded: (state, { payload: { id } }) =>
      updateState(state, ['syncingItems', id], false),
    syncItemFulfilled: (state, { payload: { id, data } }) =>
      updateState(state, ['dataMap', id], data),
    syncItemRejected: (state, action) => state,
    cancelSyncItem: (state, action) => state,

    createItem: (state, action) => state,
    createItemStarted: (state, action) => updateState(state, ['creating'], true),
    createItemEnded: (state, action) => updateState(state, ['creating'], false),
    createItemFulfilled: (state, { payload: { data } }) =>
      updateState(state, ['dataMap', data.id], data),
    createItemRejected: (state, action) => state,
    cancelCreateItem: (state, action) => state,

    updateItem: (state, action) => state,
    updateItemStarted: (state, { payload: { id } }) =>
      updateState(state, ['updatingItems', id], true),
    updateItemEnded: (state, { payload: { id } }) =>
      updateState(state, ['updatingItems', id], false),
    updateItemFulfilled: (state, { payload: { id, data } }) =>
      updateState(state, ['dataMap', id], data),
    updateItemRejected: (state, action) => state,
    cancelUpdateItem: (state, action) => state,

    update: (state, action) => state,
    updateStarted: (state, { payload: { data } }) =>
      updateStateIn(state, ['updatingItems'], (sts) => {
        return data.reduce((s, d) => updateState(s, [d.id], true), sts);
      }),
    updateEnded: (state, { payload: { data } }) =>
      updateStateIn(state, ['updatingItems'], (sts) => {
        return data.reduce((s, d) => updateState(s, [d.id], false), sts);
      }),
    updateFulfilled: (state, { payload: { data } }) =>
      updateStateIn(state, ['dataMap'], (dm: object) => {
        return data.reduce((dms, d) => updateState(dms, [d.id], d), dm);
      }),
    updateRejected: (state, action) => state,
    cancelUpdate: (state, action) => state,

    remove: (state, action) => state,
    removeStarted: (state, { payload: { ids } }) =>
      updateState(state, ['removingItems'], ids),
    removeEnded: (state, { payload: { ids } }) =>
      updateState(state, ['removingItems'], []),
    removeFulfilled: (state, { payload: { ids } }) => {
      state = updateStateIn(state, ['listData'], (idList) =>
        idList.filter((id) => !ids.includes(id)),
      );
      state = updateStateIn(state, ['selectedData'], (idList) =>
        idList.filter((id) => !ids.includes(id)),
      );
      state = updateStateIn(state, ['total'], (total) => total - ids.length);
      return ids.reduce((sts, id) => removeState(sts, ['dataMap', id]), state);
    },
    removeRejected: (state, action) => state,
    cancelRemove: (state, action) => state,

    syncAll: (state, action) => state,
    syncAllStarted: (state, action) => updateState(state, ['syncingAll'], true),
    syncAllEnded: (state, action) => updateState(state, ['syncingAll'], false),
    syncAllFulfilled: (state, { payload: { data } }) =>
      updateStateIn(state, ['dataMap'], (dm) =>
        data.reduce((dmNext, d) => {
          return updateState(dmNext, [d.id], d);
        }, dm),
      ),
    syncAllRejected: (state, action) => state,
    cancelSyncAll: (state, action) => state,
  };

  if (reducers) {
    if (typeof reducers === 'function') {
      _reducers = reducers(_reducers);
    } else {
      _reducers = { ..._reducers, ...reducers } as SliceCaseReducers<object>;
    }
  }

  const fixedReducers = Object.keys(_reducers).reduce(
    (_fixedReducers, reducerName: string) => {
      let ro = _reducers[reducerName];
      if (typeof ro === 'function') {
        ro = {
          reducer: ro,
          prepare: (arg = {}) => {
            if (Array.isArray(arg)) {
              return { payload: arg };
            } else if (typeof arg === 'object') {
              const { meta, error, ...payload } = arg;
              return { payload, meta, error };
            } else {
              return { payload: arg };
            }
          },
        };
      }
      return {
        ..._fixedReducers,
        [reducerName]: ro,
      };
    },
    {},
  );

  const slice: Slice = createSlice({
    name,
    initialState: _initialState,
    reducers: fixedReducers,
    extraReducers,
  });

  // epics

  const syncEpic = (action$, state$, { get }) => {
    return action$.pipe(
      filter(slice.actions.sync.match),
      throttleTime(ajaxThrottleTime),
      mergeMap<PayloadAction<any, string, any, any>, any>(({ payload, meta }) => {
        const { searchData, moduleParams } = state$.value[name];
        return get(resourceUrl, {
          ...searchData,
          resource_type: resourceType,
          ...moduleParams,
          _onlycount: 1,
        }).pipe(
          mergeMap((ar: AjaxResponse<number>) => {
            const count = ar.response;
            const { current, pageSize, orderby } = state$.value[name];
            return combineLatest([
              of(count),
              get(resourceUrl, {
                ...searchData,
                resource_type: resourceType,
                ...moduleParams,
                limit: pageSize,
                offset: pageSize * (current - 1),
                orderby,
              }),
            ]);
          }),
          map(([count, arn]) => {
            const { response } = arn;
            meta?.done?.(arn);
            return slice.actions.syncFulfilled({ count, data: response });
          }),
          startWith(slice.actions.syncStarted({})),
          takeUntil(action$.pipe(filter(slice.actions.cancelSync.match))),
          catchError((err) => {
            meta?.fail?.(err);
            return of(slice.actions.syncRejected({ error: err }));
          }),
          endWith(slice.actions.syncEnded({})),
        );
      }),
    );
  };

  const syncItemEpic = (action$, state$, { get }) => {
    return action$.pipe(
      filter(slice.actions.syncItem.match),
      mergeMap<PayloadAction<any, string, any, any>, any>(({ payload: { id }, meta }) => {
        return get(resourceUrl, { resource_type: resourceType, id }).pipe(
          map((ar: AjaxResponse<object[]>) => {
            const { response } = ar;
            meta?.done?.(ar);
            return slice.actions.syncItemFulfilled({ id, data: response[0] });
          }),
          startWith(slice.actions.syncItemStarted({ id })),
          takeUntil(
            action$.pipe(
              filter(
                ({ type, payload }) =>
                  type === slice.actions.cancelSyncItem.type && payload?.id === id,
              ),
            ),
          ),
          catchError((err) => {
            meta?.fail?.(err);
            return of(slice.actions.syncItemRejected({ error: err, id }));
          }),
          endWith(slice.actions.syncItemEnded({ id })),
        );
      }),
    );
  };

  const createItemEpic = (action$, state$, { post }) => {
    return action$.pipe(
      filter(slice.actions.createItem.match),
      throttleTime(ajaxThrottleTime),
      mergeMap<PayloadAction<any, string, any, any>, any>(
        ({ payload: { data }, meta }) => {
          return post(resourceUrl, {
            resource_type: resourceType,
            attrs: [data],
          }).pipe(
            map((ar: AjaxResponse<object[]>) => {
              const { response } = ar;
              meta?.done?.(ar);
              return slice.actions.createItemFulfilled({ data: response[0] });
            }),
            startWith(slice.actions.createItemStarted({})),
            takeUntil(action$.pipe(filter(slice.actions.cancelCreateItem.match))),
            catchError((err: AjaxResponse<any>) => {
              meta?.fail?.(err);
              return of(slice.actions.createItemRejected({ data, error: err }));
            }),
            endWith(slice.actions.createItemEnded({})),
          );
        },
      ),
    );
  };

  const updateItemEpic = (action$, state$, { put }) => {
    return action$.pipe(
      filter(slice.actions.updateItem.match),
      throttleTime(ajaxThrottleTime),
      mergeMap<PayloadAction<any, string, any, any>, any>(
        ({ payload: { id, data }, meta }) => {
          const storeData = getState(state$.value, [slice.name, 'dataMap', id], {});
          return put(resourceUrl, {
            resource_type: resourceType,
            attrs: [{ ...storeData, ...data, id }],
          }).pipe(
            map((ar: AjaxResponse<object[]>) => {
              const { response } = ar;
              meta?.done?.(ar);
              return slice.actions.updateItemFulfilled({ data: response[0], id });
            }),
            startWith(slice.actions.updateItemStarted({ id })),
            takeUntil(
              action$.pipe(
                filter(
                  ({ type, payload }) =>
                    type === slice.actions.cancelUpdateItem.type && payload?.id === id,
                ),
              ),
            ),
            catchError((err: AjaxResponse<any>) => {
              meta?.fail?.({ error: err, id, data });
              return of(slice.actions.updateItemRejected({ id, data, error: err }));
            }),
            endWith(slice.actions.updateItemEnded({ id })),
          );
        },
      ),
    );
  };

  const updateEpic = (action$, state$, { put }) => {
    return action$.pipe(
      filter(slice.actions.update.match),
      throttleTime(ajaxThrottleTime),
      mergeMap<PayloadAction<any, string, any, any>, any>(
        ({ payload: { data }, meta }) => {
          const storeData = getStateIn(state$.value, [slice.name, 'dataMap'], (dm) => {
            return data.map((d) => ({ ...dm[d.id], ...d }));
          });
          return put(resourceUrl, {
            resource_type: resourceType,
            attrs: [...storeData],
          }).pipe(
            map((ar: AjaxResponse<object[]>) => {
              const { response } = ar;
              meta?.done?.(ar);
              return slice.actions.updateFulfilled({ data: response });
            }),
            startWith(slice.actions.updateStarted({ data })),
            takeUntil(action$.pipe(filter(slice.actions.cancelUpdate.match))),
            catchError((err: AjaxResponse<any>) => {
              meta?.fail?.(err);
              return of(slice.actions.updateRejected({ data, error: err }));
            }),
            endWith(slice.actions.updateEnded({ data })),
          );
        },
      ),
    );
  };

  const removeEpic = (action$, state$, { del }) => {
    return action$.pipe(
      filter(slice.actions.remove.match),
      throttleTime(ajaxThrottleTime),
      mergeMap<PayloadAction<any, string, any, any>, any>(
        ({ payload: { ids }, meta }) => {
          const attrs = ids.map((id) => ({ id }));
          return del(resourceUrl, { resource_type: resourceType, attrs }).pipe(
            map((ar: AjaxResponse<object[]>) => {
              const { response } = ar;
              meta?.done?.(ar);
              return slice.actions.removeFulfilled({ ids, data: response });
            }),
            startWith(slice.actions.removeStarted({ ids })),
            takeUntil(action$.pipe(filter(slice.actions.cancelRemove.match))),
            catchError((err: AjaxResponse<any>) => {
              meta?.fail?.(err);
              return of(slice.actions.removeRejected({ ids, error: err }));
            }),
            endWith(slice.actions.removeEnded({ ids })),
          );
        },
      ),
    );
  };

  const syncAllEpic = (action$, state$, { get }) => {
    return action$.pipe(
      filter(slice.actions.syncAll.match),
      throttleTime(ajaxThrottleTime),
      mergeMap<PayloadAction<any, string, any, any>, any>(({ meta }) => {
        const { moduleParams } = state$.value[name];
        return get(resourceUrl, { resource_type: resourceType, ...moduleParams }).pipe(
          map((ar: AjaxResponse<object[]>) => {
            const { response } = ar;
            meta?.done?.(ar);
            return slice.actions.syncAllFulfilled({ data: response });
          }),
          startWith(slice.actions.syncAllStarted({})),
          takeUntil(action$.pipe(filter(slice.actions.cancelSyncAll.match))),
          catchError((err: AjaxResponse<any>) => {
            meta?.fail?.({ error: err });
            return of(slice.actions.syncAllRejected({ error: err }));
          }),
          endWith(slice.actions.syncAllEnded({})),
        );
      }),
    );
  };

  let _epics: EpicMap = {
    syncEpic,
    syncItemEpic,
    createItemEpic,
    updateItemEpic,
    updateEpic,
    removeEpic,
    syncAllEpic,
  };
  if (epics) {
    if (typeof epics === 'function') {
      _epics = epics(_epics);
    } else {
      _epics = { ..._epics, ...epics };
    }
  }

  const epicList = Object.keys(_epics).map((k) => _epics[k]);
  const epic = combineEpics(...epicList);

  return {
    slice,
    epic,
    name,
    actions: slice.actions,
    reducer: slice.reducer,
    selectors: _selectors,
  };
};
