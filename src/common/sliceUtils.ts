import {
  ActionReducerMapBuilder,
  createSlice,
  Slice,
  SliceCaseReducers,
  type ValidateSliceCaseReducers,
} from '@reduxjs/toolkit';
import { combineEpics, Epic } from 'redux-observable';

interface CreateModuleArgs {
  name: string;
  initialState?: object | ((obj: object) => object);
  reducers?:
    | SliceCaseReducers<object>
    | ((obj: SliceCaseReducers<object>) => SliceCaseReducers<object>);
  epics?: EpicMap | ((epics: EpicMap) => EpicMap);
  selectors?: Selectors<object> | ((obj: Selectors<object>) => Selectors<object>);
  extraReducers?:
    | ((builder: ActionReducerMapBuilder<object>) => void);
}

interface ModuleOption {
  slice: Slice;
  epic: Epic;
  name: string;
  actions: Slice['actions'];
  reducer: Slice['reducer'];
  selectors: object;
}

interface Selectors<State extends object> {
  [key: string]: ((state: State) => object) | ((a: any) => (state: State) => object);
}

interface EpicMap {
  [key: string]: Epic;
}

export const createModule = ({
  name,
  initialState = {},
  reducers,
  selectors,
  epics,
  extraReducers,
}: CreateModuleArgs): ModuleOption => {
  let _initialState: object = {};

  if (initialState) {
    if (typeof initialState === 'function') {
      _initialState = initialState(_initialState);
    } else {
      _initialState = { ..._initialState, ...initialState };
    }
  }

  let _selectors: Selectors<object> = {};

  if (selectors) {
    if (typeof selectors === 'function') {
      _selectors = selectors(_selectors);
    } else {
      _selectors = { ..._selectors, ...selectors };
    }
  }

  let _reducers: SliceCaseReducers<object> = {};

  if (reducers) {
    if (typeof reducers === 'function') {
      _reducers = reducers(_reducers);
    } else {
      _reducers = { ..._reducers, ...reducers } as SliceCaseReducers<object>;
    }
  }

  const fixedReducers = Object.keys(_reducers).reduce(
    (_fixedReducers: SliceCaseReducers<object>, reducerName: string) => {
      let ro = _reducers[reducerName];
      if (typeof ro === 'function') {
        ro = {
          reducer: ro,
          prepare: (arg = {}) => {
            if (Array.isArray(arg)) {
              return { payload: arg };
            } else if (typeof arg === 'object') {
              const { meta, error, ...payload } = arg;
              return { payload, meta, error };
            } else {
              return { payload: arg };
            }
          },
        };
      }
      return {
        ..._fixedReducers,
        [reducerName]: ro,
      };
    },
    {},
  ) as ValidateSliceCaseReducers<object, Record<string, any>>;

  const slice: Slice = createSlice({
    name,
    initialState: _initialState,
    reducers: fixedReducers,
    extraReducers,
  });

  let _epics: EpicMap = {};
  if (epics) {
    if (typeof epics === 'function') {
      _epics = epics(_epics);
    } else {
      _epics = { ..._epics, ...epics };
    }
  }

  const epicList = Object.keys(_epics).map((k) => _epics[k]);
  const epic = combineEpics(...epicList);

  return {
    slice,
    epic,
    name,
    actions: slice.actions,
    reducer: slice.reducer,
    selectors: _selectors,
  };
};
