# Redux Observable Toolbox

A suit of function to create simple redux observable module.

- 1.0 -> redux-observable 2.0
- 2.0 -> redux-observable 3.0

```bash
npm install redux-observable-toolbox
```

Useage:

```javascript
import { createModule } from 'redux-observable-toolbox/common';

export const { name, reducer, epic, actions, selectors, slice } = createModule({
    name: 'movies',
    initialState: {
    },
    selectors: {},
    reducers: {},
    epics: {},
});
```
